import logo from "./logo.svg";
import "./App.css";
import DemoJSS from "./JSS_StyledComponent/DemoJSS/DemoJSS";
import DemoTheme from "./JSS_StyledComponent/Themes/DemoTheme";

function App() {
  return (
    <div>
      {/* <DemoJSS /> */}
      <DemoTheme />
    </div>
  );
}

export default App;

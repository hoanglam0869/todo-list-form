import React, { Component } from "react";
import { Button, SmallButton } from "../Components/Button";
import { StyledLink } from "../Components/Link";
import { TextField } from "../Components/TextField";

export default class DemoJSS extends Component {
  render() {
    return (
      <div>
        <Button className="button_style" bgPrimary fontSize2x>
          Hello Lâm
        </Button>
        <SmallButton>Hello World</SmallButton>
        <StyledLink id="abc" name="abc123">
          Lâm đẹp trai
        </StyledLink>
        <TextField inputColor="purple" />
      </div>
    );
  }
}
